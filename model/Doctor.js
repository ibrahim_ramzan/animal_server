const mongoose=require("mongoose");
const Joi = require('joi');

const Doctor_schema =mongoose.Schema({

    name : {
        type : String,
        required : "Latitude is Required",
    },

    userName : {
        type : String,
        required : "Longitude is Required",
    },

    description : {
        type : String,
        required : "Image  is Required",
    },

    degree : {
        type : String,
        required : "description is Required",
    },

    time : {
        type : String,
        required : "Phone Number is Required",

    },

    Organization :  {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Organizations",
    },

    appointment : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "bookAppointment"
        }

    ],

},
{
    timestamps: true    
}

);

module.exports = mongoose.model("Doctor",Doctor_schema);