const mongoose=require("mongoose");
const Joi = require('joi');

const Emergency_schema =mongoose.Schema({

    latitude : {
        type : String,
        required : "Latitude is Required",
    },

    longitude : {
        type : String,
        required : "Longitude is Required",
    },

    image : {
        type : String,
        
    },

    status : {
        type : String,
        
    },

    description : {
        type : String,
        required : "description is Required",
    },

    phoneNo : {
        type : String,
        required : "Phone Number is Required",

    },

    notification : {
        type : Boolean,
        

    },

    User :  {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Users",
    },

    Organization :  {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Organizations",
    },

},
{
    timestamps: true    
}

);

module.exports = mongoose.model("Emergency",Emergency_schema);