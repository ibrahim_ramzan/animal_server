const mongoose=require("mongoose");
require('mongoose-type-url');

const Orgabiuzation_schema= new mongoose.Schema({

    company_name : {
        type : String,
        unique : true,
        required: true,
        require : "Company Name is Required"
    },

    email : {
        type : String,
        required : "Email is Required",
        unique : true,
        required: true,
        match : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/

    },

    password: {
        type : String,
        required: true,
        required : "Password is Required"
    },

    notification_token : {
        type : String,
        unique : true,
        
    },

    phone_no: {
        type : String,
        required: true,
        required : "Phone Number is Required"
    },

    address: {
        type : String,
        required : "Address is Required"
    },

    city: {
        type : String,
        required : "City is Required"
    },

    register_no: {
        type : String,
        required: true,
        unique : true,
        required : "Registration Number is Required"
    },

    webiste: {
        type: mongoose.SchemaTypes.Url,
        
    },

    organization_type : {
        type: String,
        
        
    },

    services: {
        type: String,
        
    },

    reset_password_token: {
        type: String
      },


    reset_password_expires: {
        type: Date
    },

    verify : {
        type : Boolean
    },


    bio : {
        type : String
    },

    

    appointment : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "bookAppointment"
        }

    ],

    doctors : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "Doctor"
        }

    ],


    Emergancy : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "Emergency"
        }

    ],

    


},

{
    timestamps:true
})

//mongooseModel.collection.dropIndex('email');
//Orgabiuzation_schema.collection.dropIndex('email');
module.exports = mongoose.model("Organizations", Orgabiuzation_schema);