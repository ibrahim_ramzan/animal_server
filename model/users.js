const mongoose=require("mongoose");
const Joi = require('joi');

const users_schema =mongoose.Schema({

   // _id: mongoose.Schema.Types.ObjectId,
    
    email: {
        type : String,
        required : "Email is Required",
        unique: true, 
        //match : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },

    userName: {
        type : String,
        required : "User Name is Required",
        unique: true
    },

    password : {
        type : String,
        required : "Password is Required"
    },

    profileImage : {
        type : String,
        
    },

    phoneNo : {
        type : String,
        
    },

    reset_password_token: {
        type: String
      },


    reset_password_expires: {
        type: Date
    },

    bio : {
        type : String,
        required : false
    },

    notification_token : {
        type : String,
    
     
    },

    Emergancy : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "Emergency"
        }

    ],

    Pet : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "Pet"
        }

    ],

    PetSell : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "PetSell"
        }

    ],

    PetDonate : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "PetDonate"
        }

    ],


    appointment : [
        {

        
        type : mongoose.Schema.Types.ObjectId,
        ref : "bookAppointment"
        }

    ],

    

},
{
    timestamps: true    
}

);

module.exports = mongoose.model("Users",users_schema);