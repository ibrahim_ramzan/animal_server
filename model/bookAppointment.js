const mongoose=require("mongoose");
require('mongoose-type-url');


const bookAppointment= new mongoose.Schema({

    Organization  :  {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Organizations",
    },

    User :  {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Users",
    },

    Doctor :  {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Doctor",
    },

    organization_name : {
        type: String,
        required : "Services is Required"
    },

    doctor_name : {
        type: String,
        required : "Doctor Name is Required"
    },

    time : {
        type: String,
        required : "Time is Required"
    },

    pet_name : {
        type: String,
        required : "Pet Name is Required"
    },

    pet_sex : {
        type: String,
        required : "Pet sex is Required"
    },

    notification : {
        type: Boolean,
       
    },

    status : {
        type : String,
        
    },


    pet_age : {
        type: String,
        required : "Pet age is Required"
    },

    pet_species : {
        type: String,
        required : "Pet species is Required"
    },

    appointment_reason : {
        type: String,
        required : "Appointment Reason is Required"
    },



},
{
    timestamps: true    
})


module.exports = mongoose.model("bookAppointment", bookAppointment);