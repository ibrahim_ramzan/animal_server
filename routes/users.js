const router= require("express").Router();
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
require("dotenv").config();
const jwt = require("jsonwebtoken");
const checkAuth = require('../middleware/check-auth');
nodemailer = require('nodemailer');
var userHandlers = require('../controllers/userController.js');
//var Emergency = require('../controllers/UserEmergency');
var multer  = require('multer')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.jpeg' + '.png' + '.jpg')
  }
})
 
var upload = multer({ storage: storage }).single('avatar')
var uploadEmergency = multer({ storage: storage }).single('Emergency')
const Users= mongoose.model("Users");
const Emergency= mongoose.model("Emergency");
const {ObjectId} = require('mongodb');


//router.route('/Emergency').post(Emergency.Emergency);





router.post('/profile', function (req, res) {
  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
    } else if (err) {
      // An unknown error occurred when uploading.
    }
    const user = Users.findById({_id : req.body.id})
    res.send(user[0])
; 
    // Everything went fine.
  })
})




router.route('/mail').post(userHandlers.sendMail);

router.route('/').get(userHandlers.index);

router.route('/auth/forgot_password').get(userHandlers.render_forgot_password_template).post(userHandlers.forgot_password);
    
router.route('/auth/reset_password').get(userHandlers.render_reset_password_template).post(userHandlers.reset_password);













module.exports =router;
