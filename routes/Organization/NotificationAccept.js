const router= require("express").Router();
const mongoose = require("mongoose");

const Organization =mongoose.model("Organizations");
const Emergency =mongoose.model("Emergency");

router.post("/NotificationAccept", async (req,res) => {
    
    const organization = await Organization.findOne({ _id : req.body.org_id});
    const emergency = await Emergency.findOne({ _id : req.body.Emer_id});

    
        if(emergency.status=="inactive")
        {
            emergency.status="active";
            emergency.notification=false;
            emergency.Organization = organization._id;
            organization.Emergancy.push(emergency._id);
            await organization.save();
            await emergency.save();
            res.send(emergency);
        }
        else
        {
            return res.status(409).json({
                message: "Not available"
              })
        }
    
})
  

  module.exports =router;