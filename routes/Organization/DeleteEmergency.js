const router= require("express").Router();
const mongoose = require("mongoose");

const Emergency= mongoose.model("Emergency");

router.post("/DeleteEmergency", async (req,res) => {
    
    const emergency = await Emergency.findOneAndDelete({
        _id : req.body.id
    });
    return res.status(401).json({
        message: "Done"
      });
    
})
  

  module.exports =router;