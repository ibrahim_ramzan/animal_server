const router= require("express").Router();
const mongoose = require("mongoose");

const Doctor =mongoose.model("Doctor");

router.get("/allDoctor", async (req,res) => {
    
    const doctor = await Doctor.find({
    });
    res.send(doctor)
    
})
  

  module.exports =router;