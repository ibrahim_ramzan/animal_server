const router= require("express").Router();
const mongoose = require("mongoose");


const bookAppointment =mongoose.model("bookAppointment");

router.get("/BookNotification", async (req,res) => {
    
    
    const BookAppointment = await bookAppointment.find();

    let count=0;
    
    for (let i=0; i<BookAppointment.length;i++)
    {
        if (BookAppointment[i].notification==true)
        {
            count++;
        }
    }
    
    return res.status(409).json({
        message: count
      })  
})
  

  module.exports =router;