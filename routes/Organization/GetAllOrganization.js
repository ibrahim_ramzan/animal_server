const router= require("express").Router();
const mongoose = require("mongoose");

const Organization =mongoose.model("Organizations");

router.get("/all", async (req,res) => {
    
    const organization = await Organization.find({
    });
    res.send(organization);
    
})
  

  module.exports =router;