const router= require("express").Router();
const mongoose = require("mongoose");

const Organization =mongoose.model("Organizations");

router.post("/getone", async (req,res) => {
    
    const organization = await Organization.findOne({ _id : req.body.id});
    return res.status(401).json({
      message: organization
    });
    
})
  

  module.exports =router;