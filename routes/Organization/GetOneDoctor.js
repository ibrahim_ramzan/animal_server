const router= require("express").Router();
const mongoose = require("mongoose");

const Doctor =mongoose.model("Doctor");

router.post("/getOneDoctor", async (req,res) => {
    
    const doctor = await Doctor.findOne({ _id : req.body.id});
    return res.status(401).json({
      message: doctor
    });
    
})
  

  module.exports =router;