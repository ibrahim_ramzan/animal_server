const router= require("express").Router();
const mongoose = require("mongoose");

const Organization =mongoose.model("Organizations");

router.post("/addDoctor", async(req, res, next) => {
    /*const organization = await Organization.findOne({ _id : req.body.id});
   
    if (organization)
    {
  
      var picked = _.filter(organization.doctor, x => x.userName === req.body.userName);
      if (picked)
      {
        return res.send("Doctor user name already exist")
      }
      organization.doctor.push({
        name : req.body.name,
        userName : req.body.userName,
        description : req.body.description,
        degree : req.body.degree,
        time : req.body.time
        
      })
      
    }
  
    return res.send(organization);*/
   
    const Doctors= mongoose.model("Doctor");
    const organization = await Organization.findOne({ _id : req.body.id});
    const newAppointment = new Doctors();
    newAppointment.name = req.body.name;
    newAppointment.userName = req.body.userName;
    newAppointment.description = req.body.description;
    newAppointment.degree = req.body.degree;
    newAppointment.time = req.body.time;
    
    
    newAppointment.Organization = req.body.id;
  
    
        
       await newAppointment.save();
    
       organization.doctors.push(newAppointment._id);
        await organization.save();
  
  
      
        return res.status(401).json({
          message: "Done"
        });
    
  })
  

  module.exports =router;