const router= require("express").Router();
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const Organization =mongoose.model("Organizations");

router.post("/signup", (req, res, next) => {

    Organization.find({company_name : req.body.company_name}).exec().then(organization => {
      if (organization.length >= 1 ) {
        for (var i=0;i<organization.length;i+=1) {
          if(organization[i].company_name === req.body.company_name)
          {
            return res.status(409).json({
              message: "Company Name exists"
            });
          }
      }
        
      }
    })
  
    Organization.find({register_no : req.body.register_no })
      .exec()
      .then(organization => {
        if ( organization.length ===0) {
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              
              return res.status(500).json({
                error: err
              });
            } else {
              console.log("ib")
              const newOrganization = new Organization({
                //_id: new mongoose.Types.ObjectId(),
                company_name : req.body.company_name,
                phone_no : req.body.phone_no,
                address : req.body.address,
                city : req.body.city,
                register_no : req.body.register_no,
                webiste : req.body.webiste,
                email: req.body.email,
                
                notification_token : req.body.token,
                bio : req.body.bio,
                password: hash
              });
              console.log(newOrganization)
              newOrganization
              .save()
                .then(result => {
                  res.status(201).json({
                    message: "Organization created"
                  });
                })
                .catch(err => {
                  res.status(500).json({
                    error: err
                  });
                });
            }
          });
         
        } else {
          
          for (var i=0;i<organization.length;i+=1) {
          }
          return res.status(409).json({
            message: "Registration Number exists"
          });
        }
      });
  });
  

  module.exports =router;