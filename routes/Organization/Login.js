const router= require("express").Router();
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Organization =mongoose.model("Organizations");

router.post("/login", (req, res, next) => {
    Organization.find({ email: req.body.email })
      .exec()
      .then(user => {
        if (user.length < 1) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: "Auth failed"
            });
          }
          if (result) {
            const token = jwt.sign(
              {
                email: user[0].email,
                userId: user[0]._id,
                company_name : user[0].company_name
              },
              process.env.TOKEN_KEY,
              {
                  expiresIn: "1h"
              }
            );
  
            
              return res.status(200).json({
                message: "Auth successful",
                organization_type : user[0].organization_type,
                
                key : user[0]._id
  
            
            
              
            });
          }
          res.status(401).json({
            message: "Auth failed"
          });
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });
  
  

  module.exports =router;