const router= require("express").Router();
const mongoose = require("mongoose");

const Organization =mongoose.model("Organizations");
const bookAppointment =mongoose.model("bookAppointment");

router.post("/NotificationAccept", async (req,res) => {
    
    const organization = await Organization.findOne({ _id : req.body.org_id});
    const BookAppointment = await bookAppointment.findOne({ _id : req.body.Book_id});

    
        if(BookAppointment.status=="inactive")
        {
            BookAppointment.status="active";
            BookAppointment.notification=false;
            BookAppointment.Organization = organization._id;
            organization.appointment.push(organization._id);
            await organization.save();
            await BookAppointment.save();
            return res.status(409).json({
                message: "done",
                data : BookAppointment
              })
        }
        else
        {
            return res.status(409).json({
                message: "Not available"
              })
        }
    
})
  

  module.exports =router;