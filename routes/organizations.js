const router= require("express").Router();
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
_ = require('lodash');
const jwt = require("jsonwebtoken");
var userHandlers = require('../controllers/organizationController.js');


const Organization =mongoose.model("Organizations");

router.route('/mail').post(userHandlers.sendMail);





router.route('/')
  .get(userHandlers.index);

router.route('/auth/forgot_password')
    .get(userHandlers.render_forgot_password_template)
    .post(userHandlers.forgot_password);
    
router.route('/auth/reset_password').get(userHandlers.render_reset_password_template).post(userHandlers.reset_password);





module.exports =router;