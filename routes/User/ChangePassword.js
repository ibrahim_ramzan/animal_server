const router= require("express").Router();
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
require("dotenv").config();


const Users= mongoose.model("Users");





router.put("/changedPassword", (req, res, next) => {
    Users.find({ _id: req.body.id })
      .exec()
      .then(async user => {
        if (user.length < 1) {
          return res.status(401).json({
            message: "id not exist"
          });
        }
        const match = await bcrypt.compare(req.body.password, user[0].password);
        if (match)
        {
            bcrypt.hash(req.body.newPassword, 10,async (err, hash) => {
                if (err) {
                  return res.status(500).json({
                    error: err
                  });
                } else {
                  const userUpdate = { password: hash };
                  
                  const editUser=await Users.findOneAndUpdate({
                      _id : req.body.id
                  },userUpdate,{
                      new : true,
                      runValidations : true
                  });
                  return res.status(409).json({
                    message: "done"
                  });
                }
              });
        }
        else 
        {
            return res.status(200).json({
                message: "Current password is not valid",
                
              });
        }

        
      
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });


/*router.put("/changedPassword", (req, res, next) => {
    Users.find({ _id: req.body.id })
      .exec()
      .then(user => {
        if (user.length < 1) {
          return res.status(401).json({
            message: "User doen not exist"
          });
        }
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
              return res.status(401).json({
                message: "Auth failed"
              });
            }
          if (result) {
  
           bcrypt.hash(req.body.newPassword, 10,async (err, hash) => {
              if (err) {
                return res.status(500).json({
                  error: err
                });
              } else {
                const userUpdate = { password: hash };
                
                const editUser=await Users.findOneAndUpdate({
                    _id : req.body.id
                },userUpdate,{
                    new : true,
                    runValidations : true
                });
               res.send("done");
              }
            });
            res.send("done");
          }
           
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: "User don't exist"
        });
      });
  });*/

  module.exports =router;