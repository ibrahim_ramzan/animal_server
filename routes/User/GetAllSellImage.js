const router= require("express").Router();
const mongoose = require("mongoose");

const PetSell= mongoose.model("PetSell");

router.post("/SellImage", async (req,res) => {
    
    const pet = await PetSell.findOne({ _id : req.body.id});
    
   
    return res.status(409).json({
      message: pet.petImage
    });
    
})
  

  module.exports =router;