const router= require("express").Router();
const mongoose = require("mongoose");

const Users= mongoose.model("Users");

router.get("/all", async (req,res) => {
    
    const users = await Users.find({
    });
    res.send(users)
    
})
  

  module.exports =router;