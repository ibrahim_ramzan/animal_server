const router= require("express").Router();
const mongoose = require("mongoose");

const Users= mongoose.model("Users");
const Pet= mongoose.model("PetTinder");

router.get("/petLike", async (req,res) => {
    const pet = await Pet.findOne({ _id : req.body.pet_id});
    const user = await Users.findOne({ _id : req.body.user_id});
    pet.Likes.push(req.body.user_id);
    await pet.save();
    return res.status(409).json({
      message: "done"
    });
    
})
  

  module.exports =router;