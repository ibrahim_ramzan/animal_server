const router= require("express").Router();
const mongoose = require("mongoose");

const Users= mongoose.model("Users");

router.put("/update", async (req,res) => {
    
    const editUser=await Users.findOneAndUpdate({
        _id : req.body.id
    },req.body,{
        new : true,
        runValidations : true
    });
   res.send(editUser);
    

})
  

  module.exports =router;