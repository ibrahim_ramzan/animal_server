const router= require("express").Router();
const mongoose = require("mongoose");
_ = require('lodash'),
path = require('path'),
require("dotenv").config();
var multer  = require('multer')


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + '.jpeg' + '.png' + '.jpg')
    },
    
  })


var ProfileImage = multer({ storage: storage }).single('ProfileImage')

const Users= mongoose.model("Users");

router.post("/profileImage", async (req,res) => {

  ProfileImage(req, res,async function (err) {
    if (err instanceof multer.MulterError) {
      res.send(err)
    } else if (err) {
      res.send("Unknown Error")
    }
    console.log(req.file.filename)
   // res.send(200)
       
       Users.find({_id: req.body.id}, function (err, user) {
        user = user[0];
       
       // user.profileImage = path.join(__dirname, '../../uploads', req.file.filename);
        user.profileImage=req.file.filename;
        
    
        user.save(function (err) {
            if(err) {
                console.error('ERROR!');
            }
            else{
              res.sendFile(path.join(__dirname, '../../uploads', req.file.filename));
              res.send(200)
              
            }
        });
    });
  })
})
  

  module.exports =router;