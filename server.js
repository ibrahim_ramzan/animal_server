const express=require("express");
require("express-async-errors")
const mongoose = require("mongoose");
require("express-async-errors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
path = require('path')
const app=express();


//database connection
require("./mongo");

//models
require("./model/index");

//middleware
app.use(bodyParser.json()).use(morgan())
//app.use(express.static('uploads'));
//app.use(express.static('./public'));
//app.use(express.static(path.join(__dirname, 'public')))

//routes
//user
app.use("/user", require("./routes/User/Login"))
app.use("/user", require("./routes/User/Signup"))
app.use("/user", require("./routes/User/getall"))
app.use("/user", require("./routes/User/ProfileEdit"))
app.use("/user", require("./routes/User/DeleteUser"))
app.use("/user", require("./routes/User/ChangePassword"))
app.use("/user", require("./routes/User/Emergency"))
app.use("/user", require("./routes/User/BookAppointment"))
app.use("/user", require("./routes/User/ProfileImage"))
app.use("/user", require("./routes/User/AddPetToTinder"))
app.use("/user", require("./routes/User/PetLike"))
app.use("/user", require("./routes/User/PetMatch"))
app.use("/user", require("./routes/User/AddPetToSell"))
app.use("/user", require("./routes/User/GetAllTinder"))
app.use("/user", require("./routes/User/AddPetToDonate"))
app.use("/user", require("./routes/User/DeleteTinder"))
app.use("/user", require("./routes/User/GetAllDonate"))
app.use("/user", require("./routes/User/GetAllSell"))
app.use("/user", require("./routes/User/UpdateDonate"))
app.use("/user", require("./routes/User/notification"))
app.use("/user", require("./routes/User/GetAllSellImage"))



//organization 
app.use("/organization",require("./routes/organizations"))
app.use("/organization",require("./routes/Organization/GetAllOrganization"))
app.use("/organization",require("./routes/Organization/Signup"))
app.use("/organization",require("./routes/Organization/Login"))
app.use("/organization",require("./routes/Organization/UpdateProfile"))
app.use("/organization",require("./routes/Organization/AddDoctor"))
app.use("/organization",require("./routes/Organization/DeleteOrganization"))
app.use("/organization",require("./routes/Organization/DeleteDoctor"))
app.use("/organization",require("./routes/Organization/UpdateDoctor"))
app.use("/organization",require("./routes/Organization/GetAllDoctor"))
app.use("/organization",require("./routes/Organization/GetOneOrganization"))
app.use("/organization",require("./routes/Organization/GetOneDoctor"))
app.use("/organization",require("./routes/Organization/NotificationAccept"))
app.use("/organization",require("./routes/Organization/Notification"))
app.use("/organization",require("./routes/Organization/SendNotification"))
app.use("/organization",require("./routes/Organization/BookNotification"))
app.use("/organization",require("./routes/Organization/BookNotificationAccept"))
app.use("/organization",require("./routes/Organization/BookSendNotification"))
app.use("/organization",require("./routes/Organization/DeleteEmergency"))

//error middleware

app.use((req,res,next)=> {
    req.status = 404;
    const error=new Error("Routes not found");
    next(error);
});

//error handler


if ( app.get("env") === "production"){
    app.use((error,req,res,next) => {
        res.status (req.status || 500).send({
            message: error.message
        });
    });

}

app.use((error,req,res,next) => {
    res.status (req.status || 500).send({
        message: error.message,
        stack : error.stack
    });
});




app.get("/post", (req,res)=>{
    res.send("ib")
})

app.listen(8080, ()=> {
    console.log("server is running on port 8080")
})
